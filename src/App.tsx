import React from 'react'
import Container from 'components/Container'
import Header from 'components/Header'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Header />
      </header>
      <div className="App-content">
        <Container />
      </div>
    </div>
  )
}

export default App
