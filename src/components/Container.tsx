import React, { useState } from 'react'
import WebcamCapture from './WebcamCapture'
import Prediction from './Prediction'

export interface ImageDimension {
  width: number
  height: number
}

const Container = () => {
  const [imageData, setImageData] = useState<string | null>(null)
  const [imageDimension, setImageDimension] = useState<ImageDimension>({
    width: 1280,
    height: 960,
  })

  const setImage = (
    _imageData: string | null,
    _imageDimension: ImageDimension
  ) => {
    setImageDimension(_imageDimension)
    setImageData(_imageData)
  }

  const reset = () => {
    setImageData(null)
  }

  return (
    <>
      {imageData ? (
        <Prediction
          imageData={imageData}
          imageDimension={imageDimension}
          reset={reset}
        />
      ) : (
        <WebcamCapture setImage={setImage} />
      )}
    </>
  )
}

export default Container
