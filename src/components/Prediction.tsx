import React, { useState, useCallback } from 'react'
import * as mobilenet from '@tensorflow-models/mobilenet'
import { ImageDimension } from './Container'

interface Classification {
  className: string
  probability: number
}

interface PredictionProps {
  imageData: string
  imageDimension: ImageDimension
  reset: () => void
}

const Prediction = ({ imageData, imageDimension, reset }: PredictionProps) => {
  const [loading, setLoading] = useState(true)
  const [classifications, setClassifications] = useState<Classification[]>([])

  const captureRef = useCallback(async (node) => {
    if (node !== null) {
      setLoading(true)
      const model = await mobilenet.load()
      const _classifications: Classification[] = await model.classify(node)
      setClassifications(_classifications)
      setLoading(false)
    }
  }, [])

  return (
    <>
      {imageData ? (
        <div className="prediction">
          <img
            src={imageData}
            ref={captureRef}
            className="capture"
            alt="capture"
            width={imageDimension.width}
            height={imageDimension.height}
          />
          {loading ? (
            <div>Loading ...</div>
          ) : (
            <div className="prediction-result">
              <ul>
                {classifications.map((classification, index) => {
                  return (
                    <li key={index}>
                      <span>{`${(classification.probability * 100).toFixed(
                        2
                      )}% `}</span>
                      <span>{` - ${classification.className} `}</span>
                    </li>
                  )
                })}
              </ul>
              <button className="btn-white" onClick={reset}>
                Try again
              </button>
            </div>
          )}
        </div>
      ) : null}
    </>
  )
}

export default Prediction
