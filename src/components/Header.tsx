import React from 'react'
import logo from 'images/logo.svg'

const Header = () => {
  return (
    <div className="header">
      <div className="header-logo-content">
        <img className="header-logo" src={logo} alt="yoan vallet logo" />
        <span className="header-first-name">Yoan</span>
        <span className="header-last-name">Vallet</span>
      </div>
      <span>Guess it</span>
      <div className="header-social"></div>
    </div>
  )
}

export default Header
