import React, { useState, useRef, useCallback, useEffect } from 'react'
import Webcam from 'react-webcam'
import { ImageDimension } from './Container'

interface WebcamCaptureProps {
  setImage: (imageData: string | null, dimension: ImageDimension) => void
}

const WebcamCapture = ({ setImage }: WebcamCaptureProps) => {
  const [width, setWidth] = useState<number>(0)
  const [height, setHeight] = useState<number>(0)
  const [facing, setFacing] = useState<boolean>(true)

  const webcamRef = useRef() as React.MutableRefObject<
    Webcam & HTMLVideoElement
  >

  const capture = useCallback(async () => {
    if (webcamRef.current) {
      if (webcamRef.current.getCanvas() === null) {
        alert('Webcam not detected, please change it')
        return
      }
      const dataImg = webcamRef.current.getScreenshot()
      const _width: number = webcamRef.current.props.width
        ? Number(webcamRef.current.props.width)
        : 1280
      const _height: number = webcamRef.current.props.height
        ? Number(webcamRef.current.props.height)
        : 960
      setImage(dataImg, { width: _width, height: _height })
    }
  }, [webcamRef, setImage])

  const videoConstraints = {
    width: width,
    height: height,
    facingMode: facing ? 'user' : { exact: 'environment' },
  }

  useEffect(() => {
    function handleResize() {
      if (window.innerWidth / 2 > 1280 || window.innerHeight / 2 > 960) {
        setWidth(1280)
        setHeight(960)
      } else if (window.innerHeight < window.innerWidth) {
        setWidth((window.innerHeight / 1.8) * (4 / 3))
        setHeight(window.innerHeight / 1.8)
      } else {
        setWidth(window.innerWidth / 1.2)
        setHeight(window.innerWidth / 1.2 / (4 / 3))
      }
    }
    handleResize()
    window.addEventListener('resize', handleResize)
    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return (
    <>
      <Webcam
        audio={false}
        width={width}
        height={height}
        ref={webcamRef}
        screenshotFormat="image/jpeg"
        videoConstraints={videoConstraints}
      />
      <div className="onoffswitch">
        <input
          type="checkbox"
          name="onoffswitch"
          className="onoffswitch-checkbox"
          id="myonoffswitch"
          checked={facing}
          onChange={() => setFacing(!facing)}
        />
        <label className="onoffswitch-label" htmlFor="myonoffswitch">
          <span className="onoffswitch-inner"></span>
          <span className="onoffswitch-switch"></span>
        </label>
      </div>
      <button className="btn-white" onClick={capture}>
        Capture photo
      </button>
    </>
  )
}

export default WebcamCapture
